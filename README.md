# Flaskをnoseでテストするサンプル

```
# 起動（10080ポートでテスト対象のサイトにアクセスできます）
docker-compose up -d

# テスト
docker-compose exec flask nosetests
```
