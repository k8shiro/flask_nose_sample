from nose.tools import ok_, eq_
import server

server.testing = True
client = server.app.test_client()


def test_get_top():
   res = client.get('/')
   eq_(200, res.status_code)

def test_post_top():
    res = client.post(
            '/', 
            data=dict(
                x=123,
                y=123
            ))
    ok_("Answer: 246" in res.data.decode('utf-8'))
